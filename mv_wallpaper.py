from PIL import Image
from os.path import expanduser, expandvars, isdir, isfile
from os.path import split as path_split
from os import remove as remove_file
from argparse import ArgumentParser
from argparse import Action as ParsingAction
from enum import Enum, auto
from glob import iglob
from itertools import chain
from sys import stderr
from shutil import move as move_file
from shutil import Error as shutil_Error

MAX_DEVIATION = 1.0/2.3


class Resolution(Enum):
    r720p = 720
    r1080p = 1080
    r4k = 2160
    r2160p = 2160


class ParseResolutionAction(ParsingAction):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, "r" + values)


class AspectRatio(Enum):
    ar16_9 = 16.0/9.0


class ParseAspectRatioAction(ParsingAction):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, "ar" + values)


def parse_cli_args():
    parser = ArgumentParser(
        description="Filter a set of files for images fitting to be wallpapers. Then moves them to a destination directory.")
    parser.add_argument("input_files",
                        nargs="+",
                        help="A glob or list of files to filter.")
    parser.add_argument("output_dir",
                        nargs=1,
                        help="The directory to move fitting files to.")
    parser.add_argument("-r", "--resolution",
                        default=Resolution.r1080p.name,
                        choices=list(
                            map(lambda res: res.name[1:], list(Resolution))),
                        action=ParseResolutionAction,
                        help="The minimal resolution of a wallpaper.")
    parser.add_argument("-a", "--aspect_ratio",
                        default=AspectRatio.ar16_9.name,
                        choices=list(
                            map(lambda ar: ar.name[2:], list(AspectRatio))),
                        action=ParseAspectRatioAction,
                        help="The aspect ratio of the image.")
    parser.add_argument("-f", "--force",
                        action="store_true",
                        help="Force overwriting existing files.")
    args = parser.parse_args()

    if not isdir(args.output_dir[0]):
        print("Error: '" + args.output_dir[0] +
              "' is a not a directory.", file=stderr)
        exit(1)

    return args


def build_file_iter(paths):
    files = iter([])
    # iglob also filters for whether the file exists
    for path in paths:
        files = chain(files, iglob(expand_path(path)))
    return files


def expand_path(path):
    path = expandvars(path)
    path = expanduser(path)
    return path


def is_fitting_wallpaper(filepath, min_resolution, aspect_ratio):
    min_resolution = Resolution[min_resolution].value
    aspect_ratio = AspectRatio[aspect_ratio].value

    try:
        (width, height) = read_dimensions(filepath)

        # In case the file has no information it's probably not a valid picture.
        if width == 0 and height == 0:
            return False

        resolution_ok = height >= min_resolution
        aspect_ratio_ok = abs(float(width)/float(height) -
                              aspect_ratio) < MAX_DEVIATION

        if not resolution_ok and aspect_ratio_ok:
            print("Note: '" + filepath +
                "' has correct aspect ratio but the resolution is too low.")

        return resolution_ok and aspect_ratio_ok
    except IsADirectoryError as dir_err:
        print("Warning: '" + filepath +
              "' is a directory. Ignoring...")
        return False


def read_dimensions(filename):
    try:
        img = Image.open(filename)
        return img.size
    except OSError as not_img_err:
        print("Warning: '"+ filename+ "' is not a valid image file. Ignoring...", file=stderr)
        return (0, 0)


def move_files(file_paths, destination, force_overwrite):
    amt_moved = 0

    for file_path in file_paths:
        destination_file_path = destination + "/" + path_split(file_path)[1]

        if not force_overwrite and isfile(destination_file_path):
            choice = input("'"+destination_file_path +
                           "' already exists. Do you wish to overwrite it? [y/N] ").lower()
            if choice == "y":
                remove_file(destination_file_path)
        elif force_overwrite and isfile(destination_file_path):
            remove_file(destination_file_path)

        try:
            move_file(file_path, destination)
            amt_moved += 1
        except shutil_Error as _:
            pass

    print("Finished! Moved", amt_moved, "file(s).")


def main():
    cli_args = parse_cli_args()
    input_files = build_file_iter(cli_args.input_files)
    fitting_wallpapers = filter(
        lambda file, res=cli_args.resolution, asp_rat=cli_args.aspect_ratio:
            is_fitting_wallpaper(file, res, asp_rat),
        input_files)
    move_files(fitting_wallpapers, cli_args.output_dir[0],
               cli_args.force)


main()
